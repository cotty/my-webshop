<?php
	Class ProductController extends Controller
	{
		public function index()
		{
			$controller = new ProductModel;
			$list = $controller->listProduct();
			$this->view_admin("Product/list",array('list'=>$list));
		}
		public function add()
		{
			$controller = new ProductModel;
			$listType = $controller->getlistTypeProduct();
			$count =0;
			//check post
			if(isset($_POST['Add']))
			{
				foreach ($listType as $value) {
					if($value->name == $_POST['name'])
					{
						$count++;
					}
				}
				if($count == 0 && $_POST['name']!='' && $_POST['pricenews']!='' && $_POST['chatlieu']!='' && $_POST['xuatxu']!='' && $_POST['price']!='' &&$_POST['noibat']!='' &&$_POST['typeproduct_id']!='' && $_POST['detail']!= '' &&$_SESSION['token'] == $_POST['token'] )
				{
					$name = $_POST['name'];
					$detail = $_POST['detail'];
					$price = $_POST['price'];
					$pricenews = $_POST['pricenews'];
					$chatlieu = $_POST['chatlieu'];
					$xuatxu = $_POST['xuatxu'];
					$noibat = $_POST['noibat'];
					$typeproduct_id = $_POST['typeproduct_id'];
					$idProduct = $controller->addProduct($name,$detail,$price,$pricenews,$chatlieu,$xuatxu,$noibat,$typeproduct_id);
					// produc detail
					$sl = $_POST['product_detail'];
					for($i = 1; $i<=$sl;$i++)
					{
						$size = $_POST['size'.$i.''];
						$color = $_POST['color'.$i.''];
						$quantity = $_POST['quantity'.$i.''];
						if($size != ''&& $color != '' && $quantity != '')
						{
							$controller->addProductDetail($size,$color,$quantity,$idProduct);
						}
					}
					
					$sl_image = $_POST['product_image'];
					for($j = 1;$j<=$sl_image;$j++)
					{
						$target = 'public/img/product/'.basename($_FILES['image'.$j.'']['name']);
						$image = $_FILES['image'.$j.'']['name'];
						$noibat = $_POST['noibatImage'.$j.''];
						$imageFileType = $_FILES['image'.$j.'']['type'];
						if(file_exists($target))
						{
							echo $target;
							die();
							$_SESSION['error_upload_file'] = 'File Da Ton Tai !';
						}else{
							$controller->addImageProduct($image,$noibat,$idProduct);
							move_uploaded_file($_FILES['image'.$j.'']['tmp_name'],$target);
							$_SESSION['success_upload_file'] = ' Success';
						}
					}
					$_SESSION['add_product_success'] = " Thêm Sản phảm Thành Công !";
					$_SESSION['add_product_success_time'] = time();
					header('location:index.php?c=admin&c2=Product&a=index');
					//tiep tuc
					unset($_SESSION['add_product_error']);
				}else{
					$_SESSION['add_product_error'] = 'Sản Phẩm Đã Tồn Tài';
				}
			}
			$this->view_admin("Product/add",array('listType'=>$listType));
		}
		public function update($id)
		{
			$controller = new ProductModel;
			$product = $controller->getProductById($id);
			$product_detail = $controller->getProductDetailByIdProduct($id);
			$image = $controller->getImageByIdProduct($id);
			$listType = $controller->getlistTypeProduct();

			if(isset($_POST['update']))
			{
				$count =0 ;
				foreach ($listType as $value) {
					if($value->name == $_POST['name'])
					{
						$count++;
					}
				}
				if($count == 0 && $_POST['name']!='' && $_POST['pricenews']!='' && $_POST['chatlieu']!='' && $_POST['xuatxu']!='' && $_POST['price']!='' &&$_POST['noibat']!='' &&$_POST['typeproduct_id']!='' && $_POST['detail']!= '' &&$_SESSION['token'] == $_POST['token'] )
				{
					$name = $_POST['name'];
					$detail = $_POST['detail'];
					$price = $_POST['price'];
					$pricenews = $_POST['pricenews'];
					$chatlieu = $_POST['chatlieu'];
					$xuatxu = $_POST['xuatxu'];
					$noibat = $_POST['noibat'];
					$typeproduct_id = $_POST['typeproduct_id'];
					$controller->updateProduct($product->id,$name,$detail,$price,$pricenews,$chatlieu,$xuatxu,$noibat,$typeproduct_id);
					// produc detail
					$sl = $_POST['product_detail'];
					for($i = 1; $i<=$sl;$i++)
					{
						$idProduct_detail = $_POST['idpdd'.$i.''];
						$size = $_POST['size'.$i.''];
						$color = $_POST['color'.$i.''];
						$quantity = $_POST['quantity'.$i.''];
						if($size != ''&& $color != '' && $quantity != '')
						{
							$controller->updateProductDetail($size,$color,$quantity,$id,$idProduct_detail);
						}
					}
					
					$sl_image = $_POST['product_image'];
					for($j = 1;$j<=$sl_image;$j++)
					{
						$target = 'public/img/product/'.basename($_FILES['image'.$j.'']['name']);
						$image = $_FILES['image'.$j.'']['name'];
						$idimage = $_POST['idImage'.$j.''];
						$noibat = $_POST['noibatImage'.$j.''];
						$imageFileType = $_FILES['image'.$j.'']['type'];
						if(file_exists($target))
						{
							$_SESSION['error_upload_file'] = 'File Da Ton Tai !';
						}else{
							$controller->updateImage($image,$noibat,$id,$idimage);
							move_uploaded_file($_FILES['image'.$j.'']['tmp_name'],$target);
							$_SESSION['success_upload_file'] = ' Success';
						}
					}
					$_SESSION['update_product_success'] = " Sửa Sản phảm Thành Công !";
					$_SESSION['update_product_success_time'] = time();
					//tiep tuc
					header('location:index.php?c=admin&c2=Product&a=index');
					unset($_SESSION['add_product_error']);
				}else{
					$_SESSION['add_product_error'] = 'Sản Phẩm Đã Tồn Tài';
				}
			}
			$this->view_admin("Product/update",array('listType'=>$listType,'product'=>$product,'product_detail'=>$product_detail,'image'=>$image));
		}
		public function delete($id)
		{
			$controller = new ProductModel;
			$controller->deleteProduct($id);
			$_SESSION['delete_product'] = ' Xóa Sản Phẩm Thành Công';
			$_SESSION['delete_product_time'] = time();
			header('location:index.php?c=admin&c2=Product&a=index');
		}
	}
	?>
