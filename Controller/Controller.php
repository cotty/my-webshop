<?php
Class Controller{
	public function view($view,$data = array())
	{
		  require ("View/Layout/header.php");
		  require ("View/Layout/menu.php");
		  require ("view/Layout/$view.php");
		  require ('view/Layout/footer.php');
	}
	public function view_admin($view,$data = array())
	{
		if($_SESSION['user']->quyen == 1)
		{
			require("View/Admin/Layout/header.php");
			require("View/Admin/Layout/menu.php");
			require("View/Admin/$view.php");
			require("view/Admin/Layout/footer.php");
		}else{
			header('location:index.php');
		}
		
	}
}
?>