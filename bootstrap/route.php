<?php
	session_start();
	require 'config.php';
	require 'Model/Database.php';
	require 'Controller/Controller.php';
	require 'Model/Model.php';

	 Class route {
	 	public function __construct()
	 	{
	 		Global $controller_part,$view_part,$model_part;
	 		$this->controller_part = $controller_part;
	 		$this->view_part = $view_part;
	 		$this->model_part = $model_part;
	 	}
	 	public function init()
	 	{
	 		$controller = isset($_GET['c'])?$_GET['c']:'HomePage';

	 		if($controller == 'admin')
	 		{
	 			$controller2 = isset($_GET['c2'])?$_GET['c2']:'GroupProduct';
	 			if(!file_exists("$this->controller_part/$controller2".'Controller'.".php"))
	 			{
	 				require("$this->controller_part/Error404.php");
		 			return;
	 			}
				require ("$this->controller_part/$controller2".'Controller'.".php");
		 		require ("$this->model_part/$controller2".'Model'.".php");
	 			$cc = $controller2.'Controller';
		 		if(!class_exists($cc))
		 		{	
		 			require ("$this->controller_part/Error404.php");
		 			return;
		 		}
		 		$c = new $cc;
		 		$action = isset($_GET['a'])?$_GET['a']:'index';
		 		if($action != "")
		 		{
		 			if(!method_exists($cc,$action))
		 			{
		 				require "$this->controller_part/Error404.php";
		 				return;
		 			}
		 			$id = isset($_GET['id'])?$_GET['id']:'';
		 			$c->$action($id);
		 		}
	 		}
	 		//end admin
	 		else{
	 			if(!file_exists("$this->controller_part/$controller".'Controller'.".php"))
		 		{
		 			require("$this->controller_part/Error404.php");
		 			return;
		 		}
		 		require ("$this->controller_part/$controller".'Controller'.".php");
		 		if($controller =='ShoppingCart') {
		 			require ("$this->model_part/HomePageModel.php");
		 		}else{
		 			require ("$this->model_part/$controller".'Model'.".php");
		 		}
		 		$cc = $controller.'Controller';
		 		if(!class_exists($cc))
		 		{	
		 			require ("$this->controller_part/Error404.php");
		 			return;
		 		}
		 		$c = new $cc;
		 		$action = isset($_GET['a'])?$_GET['a']:'index';
		 		if($action != "")
		 		{
		 			if(!method_exists($cc,$action))
		 			{
		 				require "$this->controller_part/Error404.php";
		 				return;
		 			}
		 			$id = isset($_GET['id'])?$_GET['id']:'';
		 			$c->$action($id);
		 		}

	 		}	 		
	 		//end
	 	}

	 }



?>