
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="icon" href="./public/img/fav-icon.png" type="image/x-icon" />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Persuit</title>

        <!-- Icon css link -->
        <link href="./public/css/font-awesome.min.css" rel="stylesheet">
        <link href="./public/vendors/line-icon/css/simple-line-icons.css" rel="stylesheet">
        <link href="./public/vendors/elegant-icon/style.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="./public/css/bootstrap.min.css" rel="stylesheet">
        
        <!-- Rev slider css -->
        <link href="./public/vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="./public/vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="./public/vendors/revolution/css/navigation.css" rel="stylesheet">
        
        <!-- Extra plugin css -->
        <link href="./public/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">
        <link href="./public/vendors/bootstrap-selector/css/bootstrap-select.min.css" rel="stylesheet">
        
        <link href="./public/css/style.css" rel="stylesheet">
        <link href="./public/css/responsive.css" rel="stylesheet">
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
</script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            #list2{
                display: none;
                position: absolute;
                right: 21px;
                top: 127px;
            }
            .log li{
            padding: 5px 15px;
            margin-left:2px;
            border: 1px solid;
            color: black;
            font-size: 14px;
            text-align: center;
            float: right;
            }
            .cart2 a i span{
                font-size: 15px;
                padding: 6px 10px;
                border: 1px solid #red;
                border-radius: 24px;
                color: white;
                background: red;
                text-align: center;
                vertical-align: middle;
            }
            
        </style>
    </head>
    <body>
<!--================Top Header Area =================-->
        <div class="header_top_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="top_header_left">
                            <!-- <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" aria-label="Search">
                                <span class="input-group-btn">
                                <button class="btn btn-secondary" type="button"><i class="icon-magnifier"></i></button>
                                </span>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="top_header_middle">
                            <a href="#"><i class="fa fa-phone"></i> Call Us: <span>+84 166 957 4322</span></a>
                            <a href="#"><i class="fa fa-envelope"></i> Email: <span>ngovanquy17091997@gmail.com</span></a>
                            <img src="public/img/logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="top_right_header">
                            <ul class="header_social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                            </ul>
                            <ul class="top_right"> 
                                <li class="cart2"><a <?php if(isset($_SESSION['shopping_cart'])) echo 'href="index.php?c=ShoppingCart"';?>><i class="icon-handbag icons"><span><?=isset($_SESSION['shopping_cart'])?count($_SESSION['shopping_cart']):'0'?></span></i></a></li>
                                <li class="user"><a href="#"><i class="icon-user icons"></i></a>                    
                                </li>
                            </ul>
                             <ul class="log" id="list2">
                                <?php if(isset($_SESSION['user']))
                                    {
                                        ?>
                                         <li><a href="index.php?c=HomePage&a=account"><?=$_SESSION['user']->username?></a></li>
                                        <li><a href="index.php?c=HomePage&a=logout">Logout</a></li>
                                        <?php
                                    }else{
                                        ?>
                                        <li><a href="index.php?c=HomePage&a=login">Login</a></li>
                                        <li><a href="index.php?c=HomePage&a=register">Register</a></li>
                                
                                        <?php

                                    }
                                 ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('.user').click(function(){
                    $('#list2').toggle();
                });
            });
        </script>
        <!--================End Top Header Area =================-->
        