<?php
    $listproduct = $data['listproduct'];
?>
<section class="shopping_cart_area p_100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="cart_items" style="margin-left:10%;">
                            <h3>Your Cart Items</h3>
                            <div class="table-responsive-md" >
                                <table class="table">
                                    <tbody>
                                        <?php
                                            $totalMoney = 0;
                                            foreach ($listproduct as $value) {
                                                $totalMoney += ($value['item_pricenews']*$value['item_quantity']);
                                            ?>
                                            <tr>
                                                <th scope="row">
                                                    <a href="index.php?c=ShoppingCart&a=delete&id=<?=$value['item_id']?>" onclick="thongbao();"><img src="public/img/icon/close-icon.png" alt=""></a>
                                                </th>
                                                <td>
                                                    <div class="media">
                                                        <div class="d-flex">
                                                            <img src="public/img/product/<?=$value['item_image']?>" width="100px" height="150px" alt="">
                                                        </div>
                                                        <div class="media-body">
                                                            <h4><?=$value['item_name']?><h4>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><p class="red"><?=number_format($value['item_pricenews'])?> VNĐ</p></td>
                                                <td>
                                                    <div class="quantity">
                                                        <h6>Quantity</h6>
                                                        <div class="custom">
                                                            <button onclick="var result = document.getElementById('sst2<?=$value['item_id']?>'); var sst2 = result.value; if( !isNaN( sst2 ) &amp;&amp; sst2 > 1 ) result.value--;return window.location.href='index.php?c=ShoppingCart&a=update&id=<?=$value['item_id']?>&quantity='+result.value;" class="reduced items-count" type="button"><i class="icon_minus-06"></i></button>
                                                            <input type="text" name="qty" id="sst2<?=$value['item_id']?>" maxlength="12" value="<?=$value['item_quantity']?>" title="Quantity:" class="input-text qty">
                                                            <button onclick="var result = document.getElementById('sst2<?=$value['item_id']?>'); var sst2 = result.value; if( !isNaN( sst2 )) result.value++;return window.location.href='index.php?c=ShoppingCart&a=update&id=<?=$value['item_id']?>&quantity='+result.value;" class="increase items-count" type="button"><i class="icon_plus"></i></button>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><p><?=number_format($value['item_pricenews']*$value['item_quantity'])?> VNĐ</p></td>
                                             </tr>
                                            <?php      
                                            }
                                        ?>
                                        <tr>
                                            <th scope="row">
                                            </th>
                                        </tr>
                                        <tr class="last">
                                            <th scope="row">
                                                <img src="img/icon/cart-icon.png" alt="">
                                            </th>
                                            <td>
                                                <div class="media">
                                                    <div class="d-flex">
                                                        <h5>Total Money:</h5>
                                                    </div>
                                                    <div class="media-body">
                                                        <h5><?=number_format($totalMoney)?> VNĐ</h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><p class="red"></p></td>
                                            <td>
                                                 <button type="submit" onclick='window.location.href="index.php?c=HomePage&a=checkout";' class="btn subs_btn form-control">Proceed to checkout</button>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </section>+