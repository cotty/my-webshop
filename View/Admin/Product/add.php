 <?php 
 $listType = $data['listType'];
 $random = rand(1,1000000000);
 $_SESSION['token'] =  $random;

?>
    <div id="wrapper">
        <!-- Navigation -->
       
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST" enctype="multipart/form-data">
                             <input class="form-control" type="hidden" name="token" id="token" value="<?=$random?>" />
                            <div class="form-group">
                                <label>Product Name</label>
                                <input class="form-control" name="name" placeholder="Please Enter .." />
                            </div>
                            <div class="form-group">
                                <label>Product Detail</label>
                                <input class="form-control" name="detail" placeholder="Please Enter..." />
                            </div>
                            <div class="form-group">
                                <label>Product Price</label>
                                <input class="form-control" name="price" placeholder="Please Enter ..." />
                            </div>
                            <div class="form-group">
                                <label>Product Price News</label>
                                <input class="form-control" name="pricenews" placeholder="Please Enter ..." />
                            </div>
                            <div class="form-group">
                                <label>Chất Liệu</label>
                                <input class="form-control" name="chatlieu" placeholder="Please Enter..." />
                            </div>
                            <div class="form-group">
                                <label>Xuất Xứ</label>
                                <input class="form-control" name="xuatxu" placeholder="Please Enter..." />
                            </div>
                            <div class="form-group">
                                <label>Nổi Bật</label>
                                <label class="radio-inline">
                                    <input name="noibat" value="1" checked="" type="radio">Có
                                </label>
                                <label class="radio-inline">
                                    <input name="noibat" value="0" type="radio">Không
                                </label>
                            </div>
                             <div class="form-group">
                                <label>Type Product Parent</label>
                                <select class="form-control" name ="typeproduct_id">
                                    <option value="0">Please Choose Type Product</option>
                                    <?php 
                                        foreach ($listType as $value) {
                                            ?>
                                            <option value="<?=$value->id?>"><?=$value->name?></option>
                                            <?php
                                        }
                                     ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Quantity Product Detail</label>
                                <select class="form-control product_detail" name ="product_detail" id="product_detail">
                                    <option value="0">Please Choose Quantity Type Product Detail</option>
                                    <?php 
                                        for($i= 1;$i<7;$i++)
                                        {
                                            ?>
                                            <option value="<?=$i?>"><?=$i?></option>
                                            <?php
                                        }
                                     ?>
                                </select>
                            </div>
                            <div id="productdetail1" style="display: none;">
                                <label>Product Detail 1</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Product Color</label>
                                    <input class="form-control" name="color1" placeholder="Please Enter ..." />
                                    <label>Product Size</label>
                                    <input class="form-control" name="size1" placeholder="Please Enter ..." />
                                    <label>Product Quantity</label>
                                    <input class="form-control" name="quantity1" placeholder="Please Enter ..." />
                                </div>
                            </div>
                            <div id="productdetail2" style="display: none;">
                                <label>Product Detail 2</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Product Color</label>
                                    <input class="form-control" name="color2" placeholder="Please Enter ..." />
                                    <label>Product Size</label>
                                    <input class="form-control" name="size2" placeholder="Please Enter ..." />
                                    <label>Product Quantity</label>
                                    <input class="form-control" name="quantity2" placeholder="Please Enter ..." />
                                </div>
                            </div>
                            <div id="productdetail3" style="display: none;">
                                <label>Product Detail 3</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Product Color</label>
                                    <input class="form-control" name="color3" placeholder="Please Enter ..." />
                                    <label>Product Size</label>
                                    <input class="form-control" name="size3" placeholder="Please Enter ..." />
                                    <label>Product Quantity</label>
                                    <input class="form-control" name="quantity3" placeholder="Please Enter ..." />
                                </div>
                            </div>
                            <div id="productdetail4" style="display: none;">
                                <label>Product Detail 4</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Product Color</label>
                                    <input class="form-control" name="color4" placeholder="Please Enter ..." />
                                    <label>Product Size</label>
                                    <input class="form-control" name="size4" placeholder="Please Enter ..." />
                                    <label>Product Quantity</label>
                                    <input class="form-control" name="quantity4" placeholder="Please Enter ..." />
                                </div>
                            </div>
                            <div id="productdetail5" style="display: none;">
                                <label>Product Detail 5</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Product Color</label>
                                    <input class="form-control" name="color5" placeholder="Please Enter ..." />
                                    <label>Product Size</label>
                                    <input class="form-control" name="size5" placeholder="Please Enter ..." />
                                    <label>Product Quantity</label>
                                    <input class="form-control" name="quantity5" placeholder="Please Enter ..." />
                                </div>
                            </div>
                            <div id="productdetail6" style="display: none;">
                                <label>Product Detail 6</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Product Color</label>
                                    <input class="form-control" name="color6" placeholder="Please Enter ..." />
                                    <label>Product Size</label>
                                    <input class="form-control" name="size6" placeholder="Please Enter ..." />
                                    <label>Product Quantity</label>
                                    <input class="form-control" name="quantity6" placeholder="Please Enter ..." />
                                </div>
                            </div>
                            <div id="productdetail7" style="display: none;">
                                <label>Product Detail 7</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Product Color</label>
                                    <input class="form-control" name="color7" placeholder="Please Enter ..." />
                                    <label>Product Size</label>
                                    <input class="form-control" name="size7" placeholder="Please Enter ..." />
                                    <label>Product Quantity</label>
                                    <input class="form-control" name="quantity7" placeholder="Please Enter ..." />
                                </div>
                            </div>

                            <script type="text/javascript">
                                   $(document).ready(function(){
                                    $("#product_detail").change(function(){
                                        var sl = $("#product_detail").val();
                                        for(var i =0; i <= sl;i++)
                                        {
                                            $('#productdetail'+i).css("display","block");
                                        }
                                        for(var j = 10;j>sl;j--)
                                        {
                                             $('#productdetail'+j).css("display","none");
                                        }
                                    });
                                     $("#product_image").change(function(){
                                        var sl2 = $("#product_image").val();
                                        for(var i =0; i <= sl2;i++)
                                        {
                                            $('#productimage'+i).css("display","block");
                                        }
                                         for(var j = 10;j>sl2;j--)
                                        {
                                             $('#productimage'+j).css("display","none");
                                        }
                                    });
                                   });  
                            </script>
                            <div class="form-group">
                                <label>Quantity Product Image</label>
                                <select class="form-control product_image" name ="product_image" id="product_image">
                                    <option value="0">Please Choose Quantity Image Product</option>
                                    <?php 
                                        for($i= 1;$i<5;$i++)
                                        {
                                            ?>
                                            <option value="<?=$i?>"><?=$i?></option>
                                            <?php
                                        }
                                     ?>
                                </select>
                            </div>
                            <div id="productimage1" style="display: none;">
                                <label>Product Image 1</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Choice Iamge</label>
                                    <input  type="file" name="image1" placeholder="Please Enter ..." />
                                </div>
                                    <div class="form-group">
                                    <label>Nổi Bật</label>
                                    <label class="radio-inline">
                                        <input name="noibatImage1" value="1" checked="" type="radio">Có
                                    </label>
                                    <label class="radio-inline">
                                        <input name="noibatImage1" value="0" type="radio">Không
                                    </label>
                                </div>
                            </div>
                            <div id="productimage2" style="display: none;">
                                <label>Product Image 2</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Choice Iamge</label>
                                    <input  type="file" name="image2" placeholder="Please Enter ..." />
                                </div>
                                    <div class="form-group">
                                    <label>Nổi Bật</label>
                                    <label class="radio-inline">
                                        <input name="noibatImage2" value="1" checked="" type="radio">Có
                                    </label>
                                    <label class="radio-inline">
                                        <input name="noibatImage2" value="0" type="radio">Không
                                    </label>
                                </div>
                            </div>
                            <div id="productimage3" style="display: none;">
                                <label>Product Image 3</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Choice Iamge</label>
                                    <input  type="file" name="image3" placeholder="Please Enter ..." />
                                </div>
                                <div class="form-group">
                                    <label>Nổi Bật</label>
                                    <label class="radio-inline">
                                        <input name="noibatImage3" value="1" checked="" type="radio">Có
                                    </label>
                                    <label class="radio-inline">
                                        <input name="noibatImage3" value="0" type="radio">Không
                                    </label>
                                </div>
                            </div>
                            <div id="productimage4" style="display: none;">
                                <label>Product Image 4</label>
                                 <div class="form-group" style="margin-left:30px;">
                                    <label>Choice Iamge</label>
                                    <input  type="file" name="image4" placeholder="Please Enter ..." />
                                </div>
                                <div class="form-group">
                                    <label>Nổi Bật</label>
                                    <label class="radio-inline">
                                        <input name="noibatImage4" value="1" checked="" type="radio">Có
                                    </label>
                                    <label class="radio-inline">
                                        <input name="noibatImage4" value="0" type="radio">Không
                                    </label>
                                </div>
                            </div>
                            <button type="submit" name="Add" class="btn btn-default">Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                            <?php if (isset($_SESSION['error_add']))
                            {
                            echo '<p align="center" class="alert alert-danger">'.$_SESSION['error_add'].'</p>'; 
                                 if(time() - $_SESSION['error_add_time'] > 20)
                                {
                                    unset($_SESSION['error_add']);
                                }
                            }
                            ?>
                            
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
