 <!-- Page Content -->
<?php 
    $list = $data['list'];
 ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Category
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Detail</th>
                                <th>price</th>
                                <th>PriceNews</th>
                                <th>Chất Liệu</th>
                                <th>Xuất Xứ</th>
                                <th>Delete</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                            <?php foreach ($list as $lt): ?>
                                  <tr class="odd gradeX" align="center">
                                        <td><?=$lt->id?></td>
                                        <td><?=$lt->name?></td>
                                        <td><?=$lt->detail?></td>
                                        <td><?=$lt->price?></td>
                                        <td><?=$lt->pricenews?></td>
                                        <td><?=$lt->chatlieu?></td>
                                        <td><?=$lt->xuatxu?></td>
                                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="index.php?c=admin&c2=Product&a=delete&id=<?=$lt->id?>"> Delete</a></td>
                                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="index.php?c=admin&c2=Product&a=update&id=<?=$lt->id?>">Detail</a></td>
                                    </tr>
                            <?php endforeach ?>
                   
                        </tbody>
                    </table>
                    <?php
                        if(isset($_SESSION['add_product_success']))
                        {
                            if(time() -$_SESSION['add_product_success_time'] > 20)
                            {
                                unset($_SESSION['add_product_success']);
                            }
                            echo '<div  align="center" class="alert alert-success">'.$_SESSION['add_product_success'].'</div>'; 
                        }
                         if(isset($_SESSION['update_product_success']))
                        {
                            if(time() - $_SESSION['update_product_success_time'] > 20)
                            {
                                unset($_SESSION['update_product_success']);
                            }
                            echo '<div  align="center" class="alert alert-success">'.$_SESSION['update_product_success'].'</div>';
                        }
                        if(isset($_SESSION['delete_product']))
                        {
                            if(time()-$_SESSION['delete_product_time']>20)
                            {
                                unset($_SESSION['delete_product']);
                            }
                            echo '<div align="center" class="alert alert-success">'.$_SESSION['delete_product'].'</div>';
                        }
                    ?>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->