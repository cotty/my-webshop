 <!-- Page Content -->
 <?php 
    $menu = $data['list'];
 ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Type Product
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Name</th>
                                <th>GroupProduct ID</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach ($menu as $value) {
                                    ?>
                                     <tr class="odd gradeX" align="center">
                                        <td><?=$value->id?></td>
                                        <td><?=$value->name?></td>
                                        <td><?=$value->groupproduct_id?></td>
                                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="index.php?c=admin&c2=TypeProduct&a=delete&id=<?=$value->id?>"> Delete</a></td>
                                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="index.php?c=admin&c2=TypeProduct&a=update&id=<?=$value->id?>">Edit</a></td>
                                    </tr>

                                    <?php
                                }
                             ?>
                   
                        </tbody>
                    </table>
                    <?php
                        if(isset($_SESSION['success_add']))
                        {
                            if(time() - $_SESSION['success_add_time'] > 20)
                            {
                                unset($_SESSION['success_add']);
                            }
                            echo '<div  align="center" class="alert alert-success">'.$_SESSION['success_add'].'</div>'; 
                        }
                         if(isset($_SESSION['success_update']))
                        {
                            if(time() - $_SESSION['success_update_time'] > 20)
                            {
                                unset($_SESSION['success_update']);
                            }
                            echo '<div  align="center" class="alert alert-success">'.$_SESSION['success_update'].'</div>';
                        }
                        if(isset($_SESSION['delete_TypeProduct']))
                        {
                            if(time()-$_SESSION['delete_TypeProduct_time']>20)
                            {
                                unset($_SESSION['delete_TypeProduct']);
                            }
                            echo '<div align="center" class="alert alert-success">'.$_SESSION['delete_TypeProduct'].'</div>';
                        }
                    ?>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->