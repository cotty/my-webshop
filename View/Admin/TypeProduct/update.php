 <?php
    $row= $data['row'];
    $listgroupproduct = $data['groupproduct'];
 ?>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Type Product
                            <small>Edit</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST">  
                            <div class="form-group">
                                <label>Type Product Id</label>
                                <input class="form-control" name="txtId" value="<?=$row->id?>" disabled placeholder="Please Enter Category Name" />
                            </div>
                            <div class="form-group">
                                <label>Type Product Name</label>
                                <input class="form-control" name="txtName" value="<?=$row->name?>" placeholder="Please Enter Category Order" />
                            </div>
                            <div class="form-group">
                                  <label>Grou Product Name</label>
                                 <select class="form-control" name ="groupProductId">
                                    <option value="0">Please Choose Group Product</option>
                                    <?php 
                                        foreach ($listgroupproduct as $value)
                                        {
                                            ?>
                                             <option value="<?=$value->id?>" <?php if($value->id == $row->groupproduct_id) {echo 'selected';}?> ><?=$value->name?></option>
                                            <?php
                                        }
                                     ?>
                                        
                                    
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default" name="btnUpdate">Update</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>