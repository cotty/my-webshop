 <?php
    $row= $data['row'];
    
 ?>
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Group Product
                            <small>Edit</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST">  
                            <div class="form-group">
                                <label>Group Product Id</label>
                                <input class="form-control" name="txtId" value="<?=$row->id?>" disabled placeholder="Please Enter Category Name" />
                            </div>
                            <div class="form-group">
                                <label>Group Product Name</label>
                                <input class="form-control" name="txtName" value="<?=$row->name?>" placeholder="Please Enter Category Order" />
                            </div>
                            <button type="submit" class="btn btn-default" name="btnUpdate">Update</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                    <?php 
                         if(isset($_SESSION['error_update']))
                            {
                                if(time() - $_SESSION['error_update_time'] > 20)
                                {
                                    unset($_SESSION['error_update']);
                                }
                                echo '<div  align="center" class="alert alert-danger">'.$_SESSION['error_update'].'</div>';
                            }
                     ?>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>