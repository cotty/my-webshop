    <?php
       /* $menu = $data['list'];
        $count = 0;
        if(isset($_POST['Add']))
        {
            $name = $_POST['Name'];
            foreach ($menu as $value) {
                if($name == $value->name)
                {
                    $count++;
                }
            }
            if($count == 0)
            {
                header('location:index.php?c=admin&c2=GroupProduct&a=postAdd&id='.$name);
            }else{
                $error = 'Tên Đã Tồn Tại !' ;
            }
        }*/
    ?>
    <div id="wrapper">
        <!-- Navigation -->
       
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Group Product
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST">
                            <div class="form-group">
                                <label>Group Product Name</label>
                                <input class="form-control" name="Name" placeholder="Please Enter Category Order" />
                            </div>
                            
                            <button type="submit" name="Add" class="btn btn-default">Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                            <?php if (isset($_SESSION['error_add']))
                            {
                            echo '<p align="center" class="alert alert-danger">'.$_SESSION['error_add'].'</p>'; 
                                 if(time() - $_SESSION['error_add_time'] > 20)
                                {
                                    unset($_SESSION['error_add']);
                                }
                            }
                            ?>
                            
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
