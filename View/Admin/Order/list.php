 <!-- Page Content -->
 <?php 
    $list = $data['list'];
 ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Category
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Order ID</th>
                                <th>order_Detail ID</th>
                                <th>Product Detail ID</th>
                                <th>Product Id</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Total Money</th>
                                <th>Status order</th>
                                <th>Status Pay</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach ($list as $value) {
                                    list($orderDetail_id,$oder_quantity,$pdd_id,$pdd_color,$pdd_size,$pd_id,$pd_name,$pd_detail,$pd_price,$pd_pricenews,$pd_chatlieu,$pd_xuatxu) = explode('/',$value->SP);
                                    ?>
                                     <tr class="odd gradeX" align="center">
                                        <td><?=$value->id?></td>
                                        <td><?=$orderDetail_id?></td>
                                        <td><?=$pdd_id?></td>
                                        <td><?=$pd_id?></td>
                                        <td><?=$pd_name?></td>
                                        <td><?=$oder_quantity?></td>
                                        <td><?=$pd_pricenews?></td>
                                         <td><?=$value->total_money?></td>
                                        <td><?=$value->status_order?></td>
                                        <td><?=$value->status_pay?></td>
                                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="index.php?c=admin&c2=GroupProduct&a=delete&id=<?=$value->id?>"> Delete</a></td>
                                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="index.php?c=admin&c2=GroupProduct&a=update&id=<?=$value->id?>">Edit</a></td>
                                    </tr>

                                    <?php
                                }
                             ?>
                   
                        </tbody>
                    </table>
                    <?php
                        if(isset($_SESSION['success_add']))
                        {
                            if(time() - $_SESSION['success_add_time'] > 20)
                            {
                                unset($_SESSION['success_add']);
                            }
                            echo '<div  align="center" class="alert alert-success">'.$_SESSION['success_add'].'</div>'; 
                        }
                         if(isset($_SESSION['success_update']))
                        {
                            if(time() - $_SESSION['success_update_time'] > 20)
                            {
                                unset($_SESSION['success_update']);
                            }
                            echo '<div  align="center" class="alert alert-success">'.$_SESSION['success_update'].'</div>';
                        }
                        if(isset($_SESSION['delete_groupproduct']))
                        {
                            if(time()-$_SESSION['delete_groupproduct_time']>20)
                            {
                                unset($_SESSION['delete_groupproduct']);
                            }
                            echo '<div align="center" class="alert alert-success">'.$_SESSION['delete_groupproduct'].'</div>';
                        }
                    ?>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->