<?php
Class GroupProductModel extends Model {
	public function getList()
	{
		return $this->getAll("groupproduct");
	}
	public function addModel($name)
	{
		$sql = " INSERT INTO groupproduct(name) values (?)";
		$this->setQuery($sql);
		return $this->execute(array($name));
	}
	public function updateModel($name,$id)
	{
		$sql ="UPDATE groupproduct SET name = ? WHERE id = ?";
		$this->setQuery($sql);
		return $this->execute(array($name,$id));
	}
	public function deleteModel($id)
	{
		$sql =" DELETE FROM groupproduct WHERE id = ?";
		$this->setQuery($sql);
		return $this->execute(array($id));
	}
}
?>