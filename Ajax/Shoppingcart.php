<?php
	session_start();
	require '../Model/Database.php';
	require '../Model/Model.php';
	require '../Model/HomePageModel.php';
	$HomePageModel = new HomePageModel;
	$item_id = $_GET['id'];
	$item_quantity = $_GET['quantity'];
	$product = $HomePageModel->getProDuctOrder($item_id);
	if(isset($_SESSION['shopping_cart']))
	{
		$item_array_id = array_column($_SESSION['shopping_cart'],'item_id');
		if(!in_array($item_id,$item_array_id))
		{
			$count = count($_SESSION['shopping_cart']);
			$item_array = array(
				'item_id'=>$item_id,
				'item_quantity'=>$item_quantity,
				'item_color'=>$product->color,
				'item_size'=>$product->size,
				'item_product_id'=>$product->product_id,
				'item_name'=>$product->name,
				'item_detail'=>$product->detail,
				'item_pricenews'=>$product->pricenews,
				'item_chatlieu'=>$product->chatlieu,
				'item_xuatxu'=>$product->xuatxu,
				'item_image'=>$product->image
			);
			$_SESSION['shopping_cart'][$count] = $item_array;
			echo '<script>alert("Đã Thêm Vào Giỏ Hàng !");</script>';
		}else {
			echo '<script>alert("Sản Phẩm Đã Có Trong Giỏ Hàng !");</script>';
			echo '<script>window.location="?c=HomePage&a=order"</script>';
		}
	}else{
		$item_array = array(
				'item_id'=>$item_id,
				'item_quantity'=>$item_quantity,
				'item_color'=>$product->color,
				'item_size'=>$product->size,
				'item_product_id'=>$product->product_id,
				'item_name'=>$product->name,
				'item_detail'=>$product->detail,
				'item_pricenews'=>$product->pricenews,
				'item_chatlieu'=>$product->chatlieu,
				'item_xuatxu'=>$product->xuatxu,
				'item_image'=>$product->image
		);
		$_SESSION['shopping_cart'][0]=$item_array;
		echo '<script>alert("Đã Thêm Vào Giỏ Hàng !");</script>';
	}
?>