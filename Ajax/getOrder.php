<?php
	require '../Model/Database.php';
	require '../Model/Model.php';
	require '../Model/HomePageModel.php';
	
	$value = $_GET['value'];
	$HomePageModel = new HomePageModel;
	$id = $HomePageModel->getIdCustomerByEmail($value)->id;
	$result = $HomePageModel->getOrder($id);
	if(count($result)>0)
	{
	foreach ($result as $v) {
		list($orderDetail_id,$oder_quantity,$pdd_id,$pdd_color,$pdd_size,$pd_id,$pd_name,$pd_detail,$pd_price,$pd_pricenews,$pd_chatlieu,$pd_xuatxu) = explode('/',$v->SP);
		?>
                <tr>
                    <th scope="row">
                        <h6><?=$pd_name?></h6>
                    </th>
                    <td>
                        <div class="media">
                            <div class="d-flex">
                                <img src="img/product/cart-product/cart-1.jpg" alt="">
                            </div>
                            <div class="media-body">
                                <h4>Mens Nike Bag</h4>
                            </div>
                        </div>
                    </td>
                    <td><p><?=$pd_pricenews?></p></td>
                    <td><input type="text" placeholder="01"></td>
                    <td><p><?=$v->total_money?></p></td>
                    <td><p><?=$v->date?></p></td>
                    <td><p><?php
                    	if($v->status_order == 0)
                    	{
                    		?>Đang Chờ<?php
                    	}else{
                    		echo('Hết Hạn Đặt Hàng');
                    	}
                    ?></p></td>
                    <td><p><?php
                    	if($v->status_pay == 0)
                    	{
                    		?>Đang Thanh Toán<?php
                    	}else{
                    		echo('Hết Hạn Thanh Toán');
                    	}
                    ?></p></td>
                </tr>
		<?php
	}
	}else{
		print_r('Khong Tim Thay Du Lieu ...');
	}

?>